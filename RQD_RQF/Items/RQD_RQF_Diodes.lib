* PSPICE RQ Power Converter Diode Library 
* 05/07/2019 CERN
* Dimitri Pracht


*$**********************************************************************************


*SHORT DESCRIPTION OF THE MODULE:

*------------------------------------------------------------------------------------------------------------------------
* This module contains the diode library of the Power Converter used within the RDF/D circuit AND the cold by-pass protection diode.


*------------------------------------------------------------------------------------------------------------------------

* To get a better OVERVIEW of this module follow the links: 
 
* ---> S:\LHC\RQD_RQF\Circuit documentation\Datasheets\Diodes

*------------------------------------------------------------------------------------------------------------------------

*The full description of the circuit is available at the following link: 

* -> https://twiki.cern.ch/twiki/bin/viewauth/TEMPEPE/SectionThesis

* --> on this Link go to: D. Pracht "Multiphysics modelling of the LHC main quadrupole superconducting circuit"

* ---> within the document go to: Chapter 5 "LHC main quadrupole electrical circuit model". 

*------------------------------------------------------------------------------------------------------------------------

*$************************************************************************************

* 6V Dipole Forward bypass diode @ cold:
* No heating effect has been taken into account.
* by Emmanuele Ravaioli

.model DiodeFWD_6V D
+ IS = 10f
+ N  = 6
+ RS = 1u
+ BV = 2k
*$

*------------------------------------

.SUBCKT 1_SKN6000 1 2
D1	1	2	DFWD
RLEAK	1	2	24.9044Meg
.MODEL DFWD D
+ IS=736E-12
+ N=2
+ RS=1E-3
+ IKF=0
+ CJO=500E-12
+ M=.4
+ VJ=.75
+ ISR=1E-10
+ NR=2
+ BV=200
+ IBV=0.1
+ TT=5E-9
.ENDS

*------------------------------------
*////////////////////////////////////////////////////
* cross checking the diode parameters against the datasheet
.SUBCKT 1_SKN6000_test 1 2
+ PARAMS:
+ IS = {0}
+ N = {0}
+ RS = {0}
D1	1	2	DFWD
RLEAK	1	2	24.9044Meg

.MODEL DFWD D
*+ IS={IS}
*+ N = {N}
*+ RS = {RS}
+ IS=0.22e-5
+ N=1.5
+ RS=1.95e-5
+ IKF=0
+ CJO=500E-12
+ M=.4
+ VJ=.75
+ ISR=1E-10
+ NR=2
+ BV=200
+ IBV=0.1
+ TT=5E-9
.ENDS

*////////////////////////////////////////////////////

*----------------------------------
.SUBCKT 440CNQ030 1 2
D1	1	2	DFWD
D1A	1	2	DXTRA
D2	2	1	DLEAK
R1	1	2	2.68602Meg
.MODEL DFWD D
+ IS=0.013475484
+ N=2
+ RS=1E-3
+ IKF=0
+ XTI=2
+ EG=0.69
+ CJO=15E-9
+ M=.5
+ VJ=.6
+ ISR=1E-10
+ NR=2
+ BV=30
+ IBV=20E-3
+ TT=5E-9
.MODEL DXTRA    D  
+ IS = 1.367485E-12
+ RS = 483.7370E-6
+ N = 0.89697
+ IKF = 0.00159295
+ TT = 0
+ CJO = 0
+ VJ = 1
+ M = .5
+ EG = 0.69
+ XTI = 2
+ KF = 0
+ AF = 1
+ FC = .5
+ BV = 1E5
+ IBV = .001
.MODEL DLEAK    D  
+ IS = 34.53224E-12
+ RS = 1
+ N = 106.607
+ TT = 0
+ CJO = 0
+ VJ = 1
+ M = .5
+ EG = .69
+ XTI = 2
+ KF = 0
+ AF = 1
+ FC = .5
+ BV = 1E5
+ IBV = .001
.ENDS


*////////////////////////////////////////////////////
* cross checking the diode parameters against the datasheet



.SUBCKT 440CNQ030_test 1 2
+ PARAMS:
+ IS = {0}
+ N = {0}
+ RS = {0}

D1	1	2	DFWD
*D1A	1	2	DXTRA
D2	2	1	DLEAK
R1	1	2	2.68602Meg
.MODEL DFWD D
*+ IS={IS}
*+ N = {N}
*+ RS = {RS}


+ IS=2.35e-5
+ N=1.0
+ RS=3.82e-4
+ IKF=0
+ XTI=2
+ EG=0.69
+ CJO=14.8E-9
+ M=.5
+ VJ=.6
+ ISR=1E-10
+ NR=2
+ BV=30
+ IBV=20E-3
+ TT=5E-9

*.MODEL DXTRA    D  
*+ IS = 1.367485E-12
*+ RS = 483.7370E-6
*+ N = 0.89697
*+ IKF = 0.00159295
*+ TT = 0
*+ CJO = 0
*+ VJ = 1
*+ M = .5
*+ EG = 0.69
*+ XTI = 2
*+ KF = 0
*+ AF = 1
*+ FC = .5
*+ BV = 1E5
*+ IBV = .001

.MODEL DLEAK    D  
+ IS = 34.53224E-12
+ RS = 1
+ N = 106.607
+ TT = 0
+ CJO = 0
+ VJ = 1
+ M = .5
+ EG = .69
+ XTI = 2
+ KF = 0
+ AF = 1
+ FC = .5
+ BV = 1E5
+ IBV = .001
.ENDS


*$************************************************************************************
*----------------------------------
*$************************************************************************************

*$***Model of the Diode for the SUB-SUB-SUB module of the PS for RQ


*--------model parameters checked against datasheet-------
.SUBCKT 249NQ150 1 2
D1_DFWD		1	2	DFWD
D2_DLEAK	2	1	DLEAK
R1			1	2	125.164k
.MODEL DFWD D
+ IS=976.5751E-6
+ N=2.45
+ RS=1.85E-3
+ IKF=0
+ XTI=2
+ EG=0.69
+ CJO=6E-9
+ M=.48
+ VJ=.6
+ ISR=1E-10
+ NR=2
+ BV=150
+ IBV=6E-3
+ TT=5E-9
.MODEL DLEAK    D  
+ IS = 67.39687E-12
+ RS = 0.1
+ N = 99.6519
+ TT = 0
+ CJO = 0
+ VJ = 1
+ M = .5
+ EG = .69
+ XTI = 2
+ KF = 0
+ AF = 1
+ FC = .5
+ BV = 1E5
+ IBV = .001
.ENDS

*////////////////////////////////////////////////////

.SUBCKT 249NQ150_test_2 1 2
*+ PARAMS:
*+ IS = {0}
*+ N = {0}
*+ RS = {0}
D1_DFWD		1	2	DFWD
D2_DLEAK	2	1	DLEAK
R1 			1	2 	1G 

* R1 was not changed

.MODEL DFWD D
+ IS  = 1.5e-3
+ N   = 1.8
+ RS  = 1.0e-3
+ BV  = 150
+ IBV = 50e-1

.MODEL DLEAK    D
+ IS  = 67.39687E-20
+ RS  = 1000
+ N   = 1000

.ENDS
*////////////////////////////////////////////////////


* cross checking the diode parameters against the datasheet

*this is the model for testing

.SUBCKT 249NQ150_test 1 2
*+ PARAMS:
*+ IS = {0}
*+ N = {0}
*+ RS = {0}
D1_DFWD		1	2	DFWD
*D2_DLEAK	2	1	DLEAK
*R1			1	2	125.164k
* R1 dont has any influence of the plot

* R1 was not changed

.MODEL DFWD D
*+ IS={IS}
*+ N = {N}
*+ RS = {RS}
+ IS=9.5e-2
* N was not changed
+ N=4.8
*RS has a influence on the plot at the end, not in the beginning
+ RS=4.7E-4
* RS was not changed
*+ IKF=0
* IKF was not changed
*+ XTI=2.0
* XTI was not changed
*+ EG=0.69
* EG was not changed
*+ CJO=6E-9 
* CJO was not changed - checekd with the datasheet: is correct
*+ M=0.5
* M was not changed
*+ VJ=.6
* VJ was not changed

*+ ISR=1E-10
* ISR was not changed
*+ NR=2
* NR was not changed
+ BV=150
* BV was not changed - checekd with the datasheet: is correct
+ IBV=50
* IBV was not changed - checekd with the datasheet: is correct
*+ TT=5E-9
* TT was not changed

.MODEL DLEAK    D  
+ IS = 67.39687E-14
+ RS = 10
+ N = 99.6519
+ TT = 0
+ CJO = 0
+ VJ = 1
+ M = .9
+ EG = .69
+ XTI = 2
+ KF = 0
+ AF = 1
+ FC = .5
+ BV = 1E5
+ IBV = .001
.ENDS
*////////////////////////////////////////////////////

*$************************************************************************************



* 6V Dipole Backward bypass diode @ cold:
* No heating effect has been taken into account.
* by Emmanuele Ravaioli

.model DiodeBWD_6V D
+ IS = 10f
+ N  = 6
+ RS = 1u
+ BV = 2k


*$************************************************************************************
*Diode used in MQ-EE Grounding system


.model D1N4099 D
+ IS  = 3.3179e-6
+ N   = 3.5120
+ RS  = 1.0000e-3
+ CJO = 80.00e-12
+ M   = 0.31349
+ VJ  = 0.3905
+ ISR = 2.9061e-6
+ BV  = 6.9975
+ IBV = 0.51729
+ TT  = 312.00e-9

*$************************************************************************************

*Diode used in EE snubber branches
* created using Parts release 7.1p on 09/05/96 at 15:48

.subckt RB_EE_DiodeSnubber 1 2

D1		(1 2)	DFWD
RLEAK	(1 2)	50e3
RAUX  	(1 3)   23e-3
D2   	(3 2)   AUXILIARY

.model DFWD D
+ IS  = 10e-6
+ N   = 3
+ RS  = 155e-6
+ M   = 0.33
+ BV  = 2.5e3
+ IBV = 100e-6
+ EG  = 1.12

.model AUXILIARY D
+ IS = 1e-9
+ EG = 1.12

.ends

*$************************************************************************************

*Diode used in EE2 grounding system
* created using Parts release 7.1p on 11/03/98 
* per Microsemi estimate: Trr(est)=400ns@I=10mA, Cap@0V=80pF

.model RB_D1N4099 D
+ IS  = 3.3179e-6
+ N   = 3.5120
+ RS  = 1.0000e-3
+ CJO = 80.00e-12
+ M   = 0.31349
+ VJ  = 0.3905
+ ISR = 2.9061e-6
+ BV  = 6.9975
+ IBV = 0.51729
+ TT  = 312.00e-9

*		Microsemi	pid=1N4099	case=DO-35





*$************************************************************************************

.subckt	RB_MB_DiodeFwdBypass_6V	   p1_In	p1_Out
	d_diode (p1_In p1_Out) DiodeFWD_6V
	r1_NS (p1_In 101)  1e-03
	c1_NS (101 p1_Out) 1e-09
.ends

*$************************************************************************************

.subckt	RB_MB_DiodeBwdBypass_6V	   p1_In	p1_Out
	d_diode (p1_In p1_Out) DiodeBWD_6V
	r1_NS (p1_In 101)  1e-03
	c1_NS (101 p1_Out) 1e-09
.ends

*$************************************************************************************

.subckt	RB_MB_DiodeEE2Gnd	   p1_In	p1_Out
	d_diode (p1_In p1_Out) RB_D1N4099
	r1_NS (p1_In 101)  1e-03
	c1_NS (101 p1_Out) 1e-09
.ends

*$************************************************************************************


*$************************************************************************************


*$************************************************************************************
* 6 V Dipole Forward by-pass Diode @ cold:
* Heating effect is taken into account
* Original model by Dimitri Pracht (CERN-TE-MPE-PE) used in the RQD/RQF circuits, 2018
* Model adapted for RB by Emmanuele Ravaioli and Marvin Janitschke (CERN-TE-MPE-PE), 2021

.subckt RQ_Protection_Diode (p1_In	p1_Out)

+ PARAMS:
+ Is    = {1e-14}
+ U_VT  = {30e-3}
+ fTL   = {1/(0.00001*395*10^3)}
+ N1    = {6.0}
+ N2    = {1.2}
+ I_0   = {50E+3}
+ U_BV  = {2000}
+ I_BV  = {0.01}
*+ Rs   = {1e-6}

v_input (p1_In 1) 0

* ABM component representing the diode behaviour.
* NOTE: Legacy versions of 2/13/2024 to the right of the arrows (-->) Since the older version expressed poor numerical behaviour, the following line was changed to the line below it (just like in the RB circuit model):
* Older version 2/13/2024 --> G_ABM_1 (1 2) VALUE {IF(V(p1_In,p1_Out)>0,LIMIT(Is*(EXP(V(p1_In,p1_Out)/(V(1_N)*U_VT))-1),0,I_0),V(p1_In,p1_Out)/(U_BV/I_BV))}
G_ABM_1 (1 2) VALUE {IF(V(p1_In,p1_Out)>0,LIMIT(Is*(EXP(V(p1_In,p1_Out)/(LIMIT(N1-(N1-N2)*LIMIT(SDT(IF(V(1,2)>0,LIMIT(I(v_input)*V(1,2),0,9.9E9),0)),0,9.9E9)*fTL, N2, N1)*U_VT))-1),0,I_0),V(p1_In,p1_Out)/(U_BV/I_BV))}
R_G_ABM_1 (1 2) {1G}

v_output (2 p1_Out) 0

* Diode current times the voltage across the diode -> to get the power. P = U * I
* Older version 2/13/2024 --> E_ABM_diode_power (1_power  0) VALUE = {IF(V(1,2)>0,I(v_input)*V(1,2),0)}
* Integration of the power --> energy of the diode
* Older version 2/13/2024 --> E_ABM_diode_energy (1_energy  0) VALUE = {LIMIT(SDT(V(1_power)),0,9.9E9)}
* Calculation of the changed parameter N to N_scaled
* Older version 2/13/2024 --> E_ABM_N_Parameter_scale (1_N  0) VALUE = {LIMIT(N1-(N1-N2)*V(1_energy)*fTL, N2, N1)}

.ends


*/////////////////////////////////////////////////////////


*$************************************************************************************
*$************************************************************************************
*$************************************************************************************
*$End of library