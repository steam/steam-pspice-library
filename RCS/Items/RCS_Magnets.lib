* PSPICE 600 A Energy-Extraction Circuit
* 2019/11/14 CERN
* Emmanuele Ravaioli

*$**********************************************************************************
* This module includes a model of the MCS magnet.
*$************************************************************************************


* Subcircuit: Magnet, modeled as a simple inductor
* Parameters in the model:
* L_magnet          self-inductance of each magnet [H]
* C_ground_magnet   parasitic capacitance to ground of each magnet [F]
* R_parallel        resistance of the resistor in parallel to each magnet [Ohm]
.subckt magnet_MCS (1_pIn 1_pOut 0_gndIn 0_gndOut)
+ PARAMS:
+ L_magnet={0.798701E-3}
+ C_ground_magnet={6.24E-09}
+ R_parallel={0.08}
*
* Magnet self-inductance
L_mag (1_pIn 1_pOut) {L_magnet}
R_parallel (1_pIn 1_pOut) {R_parallel}
C_ground_a1 (1_pIn 0_gndIn) {C_ground_magnet/2}
C_ground_b1 (1_pOut 0_gndOut) {C_ground_magnet/2}
.ends


*Subcircuit: MAGNET_EQ_1_xMagnet_MCS, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
.subckt MAGNET_EQ_1_xMagnet_MCS 1 2 
+ PARAMS:
+ L_1={0.798701E-3}
+ k_I=1.000000
+ R_par=1000.000000
*
* Magnet electrical part 1
L_diff_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_diff_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_1_stim
*
* Mutual couplings
*
* Nodes to pickup the voltage across inductances
E_L_diff_1 (1_v_l_diff 0) 1 1a 1.0
.ends


* Subcircuit: Magnet, modeled with field/circuit coupling (SPICE+LEDET co-simulation), with R_parallel and C_ground
* Parameters in the model:
* L_1               self-inductance of each magnet [H]
* C_ground_magnet   parasitic capacitance to ground of each magnet [F]
* R_parallel        resistance of the resistor in parallel to each magnet [Ohm]
* k_I               factor to scale the self-inductance (default=1) [-]
* R_par             virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt xMagnet_MCS_complete (1 2 0_gndIn 0_gndOut)
+ PARAMS:
+ L_1={0.798701E-3}
+ C_ground_magnet={6.24E-09}
+ R_parallel={0.08}
+ k_I={1.000000}
+ R_par={1000}
*
* Magnet electrical part 1
L_diff_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_diff_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_1_stim
*
* Mutual couplings
*
* Nodes to pickup the voltage across inductances
E_L_diff_1 (1_v_l_diff 0) 1 1a 1.0
*
* Resistor in parallel to the magnet
R_parallel (1 2) {R_parallel}
*
* Magnet parasitic capacitance to ground
C_ground_a1 (1 0_gndIn) {C_ground_magnet/2}
C_ground_b1 (2 0_gndOut) {C_ground_magnet/2}
.ends


*$************************************************************************************
*$End of library