* PSPICE 600 A Energy-Extraction Circuit
* 2019/11/18 CERN
* Emmanuele Ravaioli

*$**********************************************************************************
* This module includes a model of the earthing system of the RCS circuit.
*$************************************************************************************


* Subcircuit: Earthing system
* Parameters in the model:
* R_fuse           resistance of the 1 A fuse [Ohm]

.subckt earthingCircuit (1_pIn 1_pOut)
+ PARAMS:
+ R_fuse={1.0}

* Virtual voltage source for current monitoring
v_virtual_in (3 1_pIn) 0
*
* 100 Ohm ground resistor
R_100  (1_pOut 1) {100}
*
*  10 Ohm ground resistor
R_10  (1 2) {10}
*
* 1 A fuse in series to the 10 Ohm resistor
R_fuse (2 3) {R_fuse}
*
* Parallel 10 kOhm resistor
R_10k  (1_pOut 3) {10E3}
*
* 100 mA current source for monitoring purposes
I_GND (1_pOut 1) {0.100}
.ends


*$************************************************************************************
*$End of library