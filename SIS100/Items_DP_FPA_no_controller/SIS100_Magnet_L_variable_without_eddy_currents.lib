* PSPICE Magnet Model
* 2021/03/16 CERN
* Dimitri Delkov

*$**********************************************************************************
* This module includes a model of the SIS100 magnet.
*$************************************************************************************
* Subcircuit: Magnet, modeled as a simple inductor
* Parameters in the model:
* L_magnet          self-inductance of each magnet [H]
* C_ground_magnet   parasitic capacitance to ground of each magnet [F]
* R_parallel        resistance of the resistor in parallel to each magnet [Ohm]

* Model explanation:
* V_SC				this voltage source has no function, it just allows a current flow
* E_SC				this voltage source represents a current that flows through the magnet
* R_1				the resistance was created to make an electrical circuit (the value is not important, can be changed)
* X_ind				this subcircuit gives the inductance which is depending on the current (on E_SC), the inductance in this model is a voltage
* R_G2				the resistance was created due to convergence reason (must be very high)
* E_ind				inductance of the magnet in volt
* R_G3				the resistance was created due to convergence reason (must be very high)
* E_mag				formula to calculate the induced voltage across a magnet
* G_mag				formula to calculate the current in the magnet
* L_mag				inductance of the magnet (constant)
* remark: 			it must be chosen between E_mag, G_mag, and L_mag 

.subckt magnet_SIS100 (1_pIn 1_pOut 0_gndIn 0_gndOut)
+ PARAMS:
+ C_ground_magnet={6.24E-09}
+ R_parallel={0.08}
+ L_magnet={0.096}
*
* Magnet model
V_SC (1a 1_pIn) 0												
E_SC 		22a 22b VALUE={I(V_SC)}
X_ind		22b 22c inductance_table
R_1		    22c 22a {1000}
R_G2		22a 0 {1G}
E_ind		23d 23e VALUE={v(22c,22b)*(1/1000000)}
R_G3		23d 0 {1G}

*L_mag 		(1a 1b) {L_magnet}
E_mag	    (1a 1_pOut) VALUE={v(23e)*DDT(I(E_mag))}
R_parallel  (1_pIn 1_pOut) {R_parallel}
C_ground_a1 (1_pIn 0_gndIn) {C_ground_magnet/2}
C_ground_b1 (1_pOut 0_gndOut) {C_ground_magnet/2}



.ends
*$************************************************************************************
*$End of library
