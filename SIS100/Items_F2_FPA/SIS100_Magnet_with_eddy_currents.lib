* PSPICE Magnet Model
* 2021/03/16 CERN
* Dimitri Delkov

*$**********************************************************************************
* This module includes a magnet model of the SIS100 QD magnet.
*$************************************************************************************
* Subcircuit: Magnet, modeled as a simple inductor
* Parameters in the model:

* C_ground_magnet   parasitic capacitance to ground of each magnet [F]
* R_parallel        resistance of the resistor in parallel to each magnet [Ohm]

* Model explanation:
* V_SC				this voltage source has no function, it just allows a current flow
* E_SC				this voltage source represents a current that flows through the magnet
* R_1				the resistance was created to make an electrical circuit (the value is not important, can be changed)
* X_ind				this subcircuit gives the inductance which is depending on the current (on E_SC), the inductance in this model is a voltage
* R_G2				the resistance was created due to convergence reason (must be very high)
* E_ind				inductance of the magnet in volt
* R_G3				the resistance was created due to convergence reason (must be very high)
* G_mag				formula to calculate the current in the magnet

.subckt magnet_SIS100 (1_pIn 1_pOut 0_gndIn 0_gndOut)
+ PARAMS:
+ C_ground_magnet={6.24E-09}
+ R_parallel={0.08}
+ L_magnet={0.000391}
+ R_PARALLEL_POLE=290
+ L_EDDY_POLE_1={800E-5}
+ TAU_EDDY_POLE_1={800E-5}
+ R_EDDY_POLE_1={L_EDDY_POLE_1/TAU_EDDY_POLE_1}
+ K_EDDY_POLE_1={0.2}
+ L_EDDY_POLE_2={10E-5}
+ TAU_EDDY_POLE_2={10E-5}
+ R_EDDY_POLE_2={L_EDDY_POLE_2/TAU_EDDY_POLE_2}
+ K_EDDY_POLE_2={.31}
+ L_EDDY_POLE_3={2.2E-5}
+ TAU_EDDY_POLE_3={2.2E-5}
+ R_EDDY_POLE_3={L_EDDY_POLE_3/TAU_EDDY_POLE_3}
+ K_EDDY_POLE_3={.54}
+ L_EDDY_POLE_4={0.5E-5}
+ TAU_EDDY_POLE_4={0.5E-5}
+ R_EDDY_POLE_4={L_EDDY_POLE_3/TAU_EDDY_POLE_3}
+ K_EDDY_POLE_4={.025}
*
* Magnet model
V_SC        (1a 1_pIn) 0												
E_SC 		22a 22b VALUE={I(V_SC)}
X_ind		22b 22c inductance_table
R_1		 	22c 22a {1000}
R_G2		22a 0 {1G}
E_ind		23d 23e VALUE={v(22c,22b)/1000000}
R_G3		23d 0 {1G}

L_mag 		(1a 1b) {L_magnet}
E_mag	    (1b 1_pOut) VALUE={v(23e)*DDT(I(E_mag))}
*G_mag		(1b 1_pOut) VALUE={SDT((1/v(23e))*(v(1a,1_pOut)))}
R_parallel  (1_pIn 1_pOut) {R_parallel}
C_ground_a1 (1_pIn 0_gndIn) {C_ground_magnet/2}
C_ground_b1 (1_pOut 0_gndOut) {C_ground_magnet/2}

* First eddy-current loop
* Eddy current equivalent loop, coupled with all inductors of Coil #1
R_EDDY_1 (01_E_L 01_E_R) {R_EDDY_POLE_1}
L_EDDY_1 (01_E_R 01_E_L) {L_EDDY_POLE_1}
R_EDDY_1_GND (01_E_L 0) {1E12}
K_E_1_1 (L_EDDY_1 L_mag) {K_EDDY_POLE_1}
* Second eddy-current loop
* Eddy current equivalent loop, coupled with all inductors of Coil #1
R_EDDY2_1 (01_E2_L 01_E2_R) {R_EDDY_POLE_2}
L_EDDY2_1 (01_E2_R 01_E2_L) {L_EDDY_POLE_2}
R_EDDY2_1_GND (01_E2_L 0) {1E12}
K_E_2_1 (L_EDDY2_1 L_mag) {K_EDDY_POLE_2}
* third eddy-current loop
* Eddy current equivalent loop, coupled with all inductors of Coil #1
R_EDDY3_1 (01_E3_L 01_E3_R) {R_EDDY_POLE_3}
L_EDDY3_1 (01_E3_R 01_E3_L) {L_EDDY_POLE_3}
R_EDDY3_1_GND (01_E3_L 0) {1E12}
K_E_3_1 (L_EDDY3_1 L_mag) {K_EDDY_POLE_3}
* fourth eddy-current loop
* Eddy current equivalent loop, coupled with all inductors of Coil #1
R_EDDY4_1 (01_E4_L 01_E4_R) {R_EDDY_POLE_4}
L_EDDY4_1 (01_E4_R 01_E4_L) {L_EDDY_POLE_4}
R_EDDY4_1_GND (01_E4_L 0) {1E12}
K_E_4_1 (L_EDDY3_1 L_mag) {K_EDDY_POLE_4}




.ends
*$************************************************************************************
*$End of library
