* PSPICE 600 A Energy-Extraction Circuit
* 2019/11/12 CERN
* Emmanuele Ravaioli

*$**********************************************************************************
* This module includes a model of the LHC 600 A energy-extraction system.
*$************************************************************************************


* Subcircuit: EE system
* Parameters in the model:
* R_EE           resistance of the energy-extraction resistor [Ohm]
* t_EE           time at which the energy-extraction switch is opened (see also opening time and branch delays below) [s]
* Opening_Time   time required to open the switch after trigger signal arrives [s]
* Delay_Branch1  opening delay of the switch in the 1st branch [s]
* Delay_Branch2  opening delay of the switch in the 2nd branch [s]
* Delay_Branch3  opening delay of the switch in the 3rd branch [s]
* C_snubber      capacitance of the snubber capacitor across the energy-extraction resistor [F]
* R_snubber      resistance in series to the snubber capacitor (lead+fuse) [Ohm]
* R_Branch       equalizing resistance in each of the three switch branches [Ohm]
* L_Branch       parasitic inductance in each of the three switch branches [H]

.subckt EE_600A (1_pIn 1_pOut)
+ PARAMS:
+ R_EE={0.7}
+ t_EE = {0}
+ Opening_Time = {1e-3}
+ Delay_Branch1={0}
+ Delay_Branch2={0}
+ Delay_Branch3={0}
+ C_snubber={0.8e-3}
+ R_snubber={1}
+ R_Branch={440.0e-6}
+ L_Branch={1.0e-6}


* Virtual voltage source for current monitoring
v_virtual_in (1_pIn 100) 0

* Parasitic resistance and inductance of the lead between circuit and energy-extraction system - Side 1
r_parasitic_Sw_in (100 101) 50e-6
l_parasitic_Sw_in (101 102)  10e-6

* Reference voltage source used to define opening voltages for all switches (1 V represents 1 ms)
v_Switch    (control 0) PULSE 0V 1000V {t_EE} ({1s})

* Switch branch 1
l1_Sw  (102 103) {L_Branch}
r1_Sw  (103 104) {R_Branch}
x_SW1  (104 105 control) EE_switch
+ PARAMS:
+ Opening_Time={Opening_Time}
+ Delay_Branch={Delay_Branch1}

* Switch branch 2
l2_Sw  (102 203) {L_Branch}
r2_Sw  (203 204) {R_Branch}
x_SW2  (204 105 control) EE_switch
+ PARAMS:
+ Opening_Time={Opening_Time}
+ Delay_Branch={Delay_Branch2}

* Switch branch 3
l3_Sw  (102 303) {L_Branch}
r3_Sw  (303 304) {R_Branch}
x_SW3  (304 105 control) EE_switch
+ PARAMS:
+ Opening_Time={Opening_Time}
+ Delay_Branch={Delay_Branch3}

* Energy-Extraction resistor (with middle-point 1_MP_GND)
R_EE_1Half (102 1_MP_GND) {0.5*R_EE}
R_EE_2Half (1_MP_GND 105) {0.5*R_EE}

* Snubber capacitor
C_snubber (102 403) {C_snubber}
R_snubber (403 105) {R_snubber}

* Parasitic resistance and inductance of the lead between circuit and energy-extraction system - Side 2
r_parasitic_Sw_out (105 106) 50e-6
l_parasitic_Sw_out (106 107)  10e-6

* Virtual voltage source for current monitoring
v_virtual_out (107 1_pOut) 0

.ends


* Sub-model of the energy-extraction switch
.subckt EE_switch 1 2 control
+ PARAMS:
+ Opening_Time={1e-3}
+ Delay_Branch={0}
S1    (1 2 control 0) xSmod
.model xSmod Vswitch  Voff={(Delay_Branch+Opening_Time)*1000} Von={Delay_Branch*1000}  Roff=1e+06 Ron=10e-06
.ends


*$************************************************************************************
*$End of library
