
* PSPICE RB EE components library
* 2015/09/17 CERN
* Lorenzo Bortot

*$************************************************************************************

*Subcircuit: RB_EE1_4poles - Full 4 poles model
.subckt	RB_EE1_4poles	1_pIn	1_pOut

*Inner Busbar
v1_BbIn_PH (1_pIn 100) 0

*Pole 1
x1_Switches (100 101) RB_EE1Switch4polesBranch
x1_Snub     (100 101) RB_EESnubber4polesBranch
l1_SwOut 	(101 102) 3.8e-6
r1_SwOut 	(102 103) 80e-6

*Pole 2
x2_Switches (100 201) RB_EE1Switch4polesBranch
x2_Snub     (200 201) RB_EESnubber4polesBranch
l2_SwOut 	(201 202) 3.8e-6
r2_SwOut 	(202 103) 80e-6

*Pole 3
l3_SwIn 	(100 301) 3.8e-6
r3_SwIn 	(301 302) 80e-6
x3_Switches (302 103) RB_EE1Switch4polesBranch
x3_Snub     (302 103) RB_EESnubber4polesBranch

*Pole 4
l4_SwIn 	(100 401) 3.8e-6
r4_SwIn 	(401 402) 80e-6
x4_Switches (402 103) RB_EE1Switch4polesBranch
x4_Snub     (402 103) RB_EESnubber4polesBranch

*Branch of extraction resistors
x1_EERes    (100 103 1_float) RB_EEResistorBranch

*Outer Busbar
v1_BbOut_PH (103 1_pOut) 0
.ends

*$************************************************************************************

*Subcircuit: RB_EE1_1poleEq -- 1 pole equivalent model
.subckt	RB_EE1_1poleEq	1_pIn	1_pOut
+ PARAMS:
+ R_EE_1 = {110e-3}
+ R_EE_2 = {55e-3}

*Inner Busbar
v1_BbIn_PH (1_pIn 100) 0

*Pole 1
x1_Switches (100 101) RB_EE1Switch1poleEqBranch
x1_Snub     (100 101) RB_EESnubber1poleEqBranch
l1_SwOut 	(101 102) 0.95e-6
r1_SwOut 	(102 103) 20e-6

*Branch of extraction resistors
x1_EERes    (100 103 1_float) RB_EEResistorBranch
+ PARAMS: R_EE_1 = {R_EE_1} R_EE_2 = {R_EE_2}

*Outer Busbar
v1_BbOut_PH (103 1_pOut) 0
.ends

*$************************************************************************************

*Subcircuit: RB_EE2_4poles - Full 4 poles model
.subckt	RB_EE2_4poles	1_pIn	1_pOut

*Inner Busbar
v1_bbIn_PH (1_pIn 100) 0

*Pole 1
x1_Switches (100 101) RB_EE2Switch4polesBranch
x1_Snub     (100 101) RB_EESnubber4polesBranch
l1_bbOut 	(101 102) 3.8e-6
r1_bbOut 	(102 103) 80e-6

*Pole 2
x2_Switches (100 201) RB_EE2Switch4polesBranch
x2_Snub     (200 201) RB_EESnubber4polesBranch
l2_bbOut 	(201 202) 3.8e-6
r2_bbOut 	(202 103) 80e-6

*Pole 3
l3_bbOut 	(100 301) 3.8e-6
r3_bbOut 	(301 302) 80e-6
x3_Switches (302 103) RB_EE2Switch4polesBranch
x3_Snub     (302 103) RB_EESnubber4polesBranch

*Pole 4
l4_bbOut 	(100 401) 3.8e-6
r4_bbOut 	(401 402) 80e-6
x4_Switches (402 103) RB_EE2Switch4polesBranch
x4_Snub     (402 103) RB_EESnubber4polesBranch

*energy extractor
x1_EERes    (100 101 1_float) RB_EEResistorBranch

*grounding line
x1_EE2Gnd   (1_float)  RB_EE2_Gnd

*Outer Busbar
v1_bbOut_PH (103 1_pOut) 0
.ends

*$************************************************************************************

*Subcircuit: RB_EE2_1poleEq - 1 pole equivalent model
.subckt	RB_EE2_1poleEq	1_pIn	1_pOut
+ PARAMS:
+ R_EE_1 = {110e-3}
+ R_EE_2 = {55e-3}

*Inner Busbar
v1_bbIn_PH (1_pIn 100) 0

*Branch 1
x1_Switches (100 101) RB_EE2Switch1poleEqBranch
x1_Snub     (100 101) RB_EESnubber1poleEqBranch
l1_bbOut 	(101 102) 1e-6
r1_bbOut 	(102 103) 20e-6

*energy extractor
x1_EERes    (100 101 1_float) RB_EEResistorBranch
+ PARAMS: R_EE_1 = {R_EE_1} R_EE_2 = {R_EE_2}

*grounding line
x1_EE2Gnd   (1_float)  RB_EE2_Gnd

*Outer Busbar
v1_bbOut_PH (103 1_pOut) 0
.ends

*$************************************************************************************
*$************************************************************************************
*$************************************************************************************

*Subcircuit: RB_EE1Switch4polesBranch
.subckt	RB_EE1Switch4polesBranch	1_pIn	1_pOut

*Inner Busbar
v1_bbIn_PH ( 1_pIn 100 ) 0

*1st switch
x_SW1   (100 101) RB_EE1Switch01_4poles
v1_Link (101 200) 0
*2nd switch
x_SW2   (200 201) RB_EE1Switch02_4poles
v2_Link (201 300) 0
*3rd switch
x_SW3   (300 301) RB_EE1Switch03_4poles
v3_Link (301 400) 0
*4th switch
x_SW4   (400 401) RB_EE1Switch04_4poles

*Outer Busbar
v1_bbOut_PH (401 1_pOut) 0
.ends 

*$************************************************************************************

*Subcircuit: RB_EE1Switch1poleEqBranch
.subckt	RB_EE1Switch1poleEqBranch	1_pIn	1_pOut

*Inner Busbar
v1_bbIn_PH ( 1_pIn 100 ) 0

*1st switch
x_SW1   (100 101) RB_EE1Switch01_1poleEq
v1_Link (101 200) 0
*2nd switch
x_SW2   (200 201) RB_EE1Switch02_1poleEq
v2_Link (201 300) 0
*3rd switch
x_SW3   (300 301) RB_EE1Switch03_1poleEq
v3_Link (301 400) 0
*4th switch
x_SW4   (400 401) RB_EE1Switch04_1poleEq

*Outer Busbar
v1_bbOut_PH (401 1_pOut) 0
.ends 

*$************************************************************************************

*Subcircuit: RB_EE2Switch4polesBranch -- Simulink Schematics Available
.subckt	RB_EE2Switch4polesBranch	1_pIn	1_pOut

*Inner Busbar
v1_bbIn_PH (1_pIn 100) 0

*1st switch
x_SW1   (100 101) RB_EE2Switch01_4poles
v1_Link (101 200) 0
*2nd switch
x_SW2   (200 201) RB_EE2Switch02_4poles
v2_Link (201 300) 0
*3rd switch
x_SW3   (300 301) RB_EE2Switch03_4poles
v3_Link (301 400) 0
*4th switch
x_SW4   (400 401) RB_EE2Switch04_4poles

*Outer Busbar
v1_bbOut_PH (401 1_pOut) 0
.ends
 
*$************************************************************************************

*Subcircuit: RB_EE2Switch1poleEqBranch -- Simulink Schematics Available
.subckt	RB_EE2Switch1poleEqBranch	1_pIn	1_pOut

*Inner Busbar
v1_bbIn_PH (1_pIn 100) 0

*1st switch
x_SW1   (100 101) RB_EE2Switch01_1poleEq
v1_Link (101 200) 0
*2nd switch
x_SW2   (200 201) RB_EE2Switch02_1poleEq
v2_Link (201 300) 0
*3rd switch
x_SW3   (300 301) RB_EE2Switch03_1poleEq
v3_Link (301 400) 0
*4th switch
x_SW4   (400 401) RB_EE2Switch04_1poleEq

*Outer Busbar
v1_bbOut_PH (401 1_pOut) 0
.ends
 
*$************************************************************************************

*Subcircuit: RB_EESnubber4polesBranch
.subckt	RB_EESnubber4polesBranch	1_pIn	1_pOut 

*Inner Busbar
v1_bbIn_PH (1_pIn 100) 0

*1st snubber RLC branch
r1_snub1 (100 101) 300e-6
l1_snub1 (101 102) 190e-9
c1_snub1 (102 103) 13.3e-3

**2st snubber four equalizing resistors
r1_snub2 (100 103) 33e3

*Snubbing Diodes
x1_diode (103 104) RB_EE_DiodeSnubber
x2_diode (104 105) RB_EE_DiodeSnubber

*3st snubber RC stabilizing circuit for diodes
r1_snub3 (103 401) 5333.34
r2_snub3 (401 105) 5333.34
r3_snub3 (103 501) 1
c1_snub3 (501 502) 15e-6
r4_snub3 (502 503) 1
c2_snub3 (503 105) 15e-6
v1_snub3_equiPot (502 401) 0
v2_snub3_equiPot (401 104) 0

*Outer Busbar
v1_bbOut_PH (105 1_pOut) 0
.ends

*$************************************************************************************

*Subcircuit: RB_EESnubber1poleEqBranch
.subckt	RB_EESnubber1poleEqBranch	1_pIn	1_pOut 

*Inner Busbar
v1_bbIn_PH (1_pIn 100) 0

*1st snubber RLC branch
r1_snub1 (100 101) 75e-6
l1_snub1 (101 102) 47.5e-9
c1_snub1 (102 103) 53.2e-3

**2st snubber
r1_snub2 (100 103) 8.25e3

*Snubbing Diodes
x1_diode (103 104) RB_EE_DiodeSnubber
x2_diode (104 105) RB_EE_DiodeSnubber

x3_diode (103 204) RB_EE_DiodeSnubber
x4_diode (204 105) RB_EE_DiodeSnubber

x5_diode (103 304) RB_EE_DiodeSnubber
x6_diode (304 105) RB_EE_DiodeSnubber

x7_diode (103 404) RB_EE_DiodeSnubber
x8_diode (404 105) RB_EE_DiodeSnubber

*3st snubber RC stabilizing circuit for diodes
r1_snub3 (103 501) 1333.34
r2_snub3 (501 105) 1333.34

r3_snub3 (103 601) 0.25
c1_snub3 (601 602) 60e-6
r4_snub3 (602 603) 0.25
c2_snub3 (603 105) 60e-6

v1_EqPot (602 501) 0
v2_EqPot (501 404) 0
v3_EqPot (404 304) 0
v4_EqPot (304 204) 0
v5_EqPot (204 104) 0

*Outer Busbar
v1_bbOut_PH (105 1_pOut) 0
.ends

*$************************************************************************************

*Subcircuit: RB_EEResistorBranch -- Simulink Schematics Available
.subckt	RB_EEResistorBranch	1_pIn	1_pOut	3_pGnd
+ PARAMS:
+ R_EE_1 = {110E-3}
+ R_EE_2 = {55E-3}

*Inner Busbar
v1_BbIn_PH (1_pIn 100) 0
l1_BbIn    (100 101) 2e-6

*resistors
*for 3.5 TeV remember to double the values
r1_EE  (101 102) 	{R_EE_1}
r2_EE  (102 103) 	{R_EE_1}
r3_EE  (101 201) 	{R_EE_2}
r4_EE  (201 103)    {R_EE_2}

c1_EE  (100 104)    1.25e-6

* port to ground
v1_Gnd (201 3_pGnd) 0

*Outer Busbar 
l1_BbOut    (103 104) 2e-6
v1_BbOut_PH (104 1_pOut) 0
.ends

*$***********************************************************************************

*Subcircuit: RB_EE_Gnd -- Simulink Schematics Available
.subckt	RB_EE2_Gnd	1_pIn
	
*Inner Busbar
v1_bbIn_PlaceHolder (1_pIn 100) 0

*Resistors
r1_Fuse1A    (100 101) 1
*r1_Fuse200mA (100 101) 17.5
* The following line was corrected in May 2021
* The previous, wrong line was "r1_gndDetect (101 0)   10k"
r1_gndDetect (100 0)   10k

r1_DiodesFuse100mA (101 201) 33
r1_gndDiodes       (201 0)   10k

*Back to back Protecting diodes
d1_gnd  (301 201) RB_D1N4099
c1_d1NS (201 401) 1nF
r1_d1NS (401 301) 1mOhm

d2_gnd  (301 0)   RB_D1N4099
c1_d2NS (301 402) 1nF
r1_d2NS (402 0)   1mOhm

r1_NS   (301 0)  100e6

*Bias voltage generator
r1_BIAS          (101 0)   10
r1_BIASFuse200mA (101 501) 33
I_BIAS           (0 501)   100m
.ends

*$************************************************************************************
*$************************************************************************************
*$************************************************************************************
*$End of library
