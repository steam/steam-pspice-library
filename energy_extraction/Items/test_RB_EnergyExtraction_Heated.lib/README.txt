This is a test for the RB_EE_EnergyExtraction_Heated.lib

the resistance of a single energy extraction can be checked with:
(V(x1_RB_EE1.x1_R_EE.101)-V(x1_RB_EE1.x1_R_EE.104))/I(x1_RB_EE1.x1_R_EE.V_monitor_in)

--> this value should be around 70mOhm, which is the case

the temperature should increase by about 100K (which is the case):
--> This can be checked with 
V(x1_RB_EE1.x1_R_EE.x1_R_EE.T_Resistor)

