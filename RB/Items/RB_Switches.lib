
* PSPICE RB switches library
* 2015/09/17 CERN
* Lorenzo Bortot

*$************************************************************************************

*$ EE1 definition of switch parameters
*1st Switch
.model w_EE1Sw01_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=260e-06 Ron=2e-06
.model w_EE1Sw01_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=65e-06  Ron=0.5e-06
*$
*2nd Switch
.model w_EE1Sw02_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=32.74e-03 Ron=2e-06
.model w_EE1Sw02_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=8.2e-03   Ron=0.5e-06
*$
*3rd Switch
.model w_EE1Sw03_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=57e-03 Ron=2e-06
.model w_EE1Sw03_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=14e-03 Ron=0.5e-06
*$
*4th Switch
.model w_EE1Sw04_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=1e6   Ron=2e-06
.model w_EE1Sw04_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=250e3 Ron=0.5e-06
*$

*$ EE2 definition of switch parameters
*1st Switch
.model w_EE2Sw01_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=260e-06 Ron=2e-06
.model w_EE2Sw01_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=65e-06  Ron=0.5e-06
*$
*2nd Switch
.model w_EE2Sw02_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=32.74e-03 Ron=2e-06
.model w_EE2Sw02_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=8.2e-03   Ron=0.5e-06
*$
*3rd Switch
.model w_EE2Sw03_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=57e-03 Ron=2e-06
.model w_EE2Sw03_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=14e-03 Ron=0.5e-06
*$
*4th Switch
.model w_EE2Sw04_4poles  ISWITCH  Ioff=0.0 Ion=0.5  Roff=1e6   Ron=2e-06
.model w_EE2Sw04_1poleEq ISWITCH  Ioff=0.0 Ion=0.5  Roff=250e3 Ron=0.5e-06
*$

*$************************************************************************************

*$ EE1 switches 1poleEq
 
*1st Switch
.subckt  RB_EE1Switch01_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw01_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_1

.ends
*2nd Switch
.subckt  RB_EE1Switch02_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw02_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_2
.ends

*3rd Switch
.subckt  RB_EE1Switch03_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw03_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_3
.ends

*4th Switch
.subckt  RB_EE1Switch04_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw04_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_4
.ends

*$ EE2 switches 1poleEq
 
*1st Switch
.subckt  RB_EE2Switch01_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw01_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_1
.ends

*2nd Switch
.subckt  RB_EE2Switch02_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw02_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_2
.ends

*3rd Switch
.subckt  RB_EE2Switch03_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw03_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_3
.ends

*4th Switch
.subckt  RB_EE2Switch04_1poleEq   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw04_1poleEq
v_SW1    (102 0)         0
r1_gate  (102 103)       100
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_4
.ends

*$************************************************************************************

*$ EE1 switches 4poles
 
*1st Switch
.subckt  RB_EE1Switch01_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw01_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_1

.ends
*2nd Switch
.subckt  RB_EE1Switch02_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw02_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_2
.ends

*3rd Switch
.subckt  RB_EE1Switch03_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw03_4polesq
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_3
.ends

*4th Switch
.subckt  RB_EE1Switch04_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE1Sw04_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE1_SWITCH_4
.ends

*$ EE2 switches 4poles
 
*1st Switch
.subckt  RB_EE2Switch01_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw01_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_1
.ends

*2nd Switch
.subckt  RB_EE2Switch02_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw02_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_2
.ends

*3rd Switch
.subckt  RB_EE2Switch03_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw03_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100 
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_3
.ends

*4th Switch
.subckt  RB_EE2Switch04_4poles   1_pIn  1_pOut
w_SW1    (1_pIn 1_pOut v_SW1) w_EE2Sw04_4poles
v_SW1    (102 0)         0
r1_gate  (102 103)       100
i1_gate  (0 103)         STIMULUS=I_GATE_EE2_SWITCH_4
.ends

*$************************************************************************************
*$************************************************************************************
*$************************************************************************************
*$End of library