
* PSPICE RB thyristors library
* 2015/09/17 CERN
* Lorenzo Bortot

*$************************************************************************************

* This component is the reference one for every thyristor:
* A new thiristor is created invoking SCR circuit with custom parameters
.subckt Scr anode gate cathode PARAMS:
+ Vdrm=400v     Vrrm=400v     Idrm=10u
+ Ih=6ma        dVdt=5e7
+ Igt=5ma       Vgt=0.7v
+ Vtm=1.7v      Itm=24
+ Ton=1u        Toff=15u

* Where:
* Vdrm =>  Forward breakover voltage
* Vrrm =>  Reverse breakdown voltage
* Idrm =>  Peak blocking current
* Ih   =>  Holding current
* dVdt =>  Critical value for dV/dt triggering
* Igt  =>  Gate trigger current
* Vgt  =>  Gate trigger voltage
* Vtm  =>  On-state voltage
* Itm  =>  On-state current
* Ton  =>  Turn-on time
* Toff =>  Turn-off time

* Main conduction path
Scr      anode   anode0  control 0       Vswitch ; controlled switch
Dak1     anode0  anode2  Dakfwd  OFF             ; SCR is initially off
Dka      cathode anode0  Dkarev  OFF
VIak     anode2  cathode                         ; current sensor

* dVdt Turn-on
Emon     dvdt0   0       TABLE {v(anode,cathode)} (0 0) (2000 2000)
CdVdt    dvdt0   dvdt1   100p                 ; displacement current
Rdlay    dvdt1   dvdt2   1k
VdVdt    dvdt2   cathode DC 0.0
EdVdt    condvdt 0       TABLE {i(vdVdt)-100p*dVdt}  (0 0 ) (.1m 10)
RdVdt    condvdt 0       1meg

* Gate
Rseries  gate    gate1   {(Vgt-0.65)/Igt}
Rshunt   gate1   gate2   {0.65/Igt}
Dgkf     gate1   gate2   Dgk
VIgf     gate2   cathode                         ; current sensor

* Gate Turn-on
Egate1   gate4   0       TABLE {i(Vigf)-0.95*Igt} (0 0) (1m 10)
Rgate1   gate4   0       1meg
Egon1    congate 0       TABLE {v(gate4)*v(anode,cathode)} (0 0) (10 10)
Rgon1    congate 0       1meg

* Main Turn-on
EItot    Itot    0       TABLE {i(VIak)+5E-5*i(VIgf)/Igt} (0 0) (2000 2000)
RItot    Itot    0       1meg
Eprod    prod    0       TABLE {v(anode,cathode)*v(Itot)} (0 0) (10 10)
Rprod    prod    0       1meg
Elin     conmain 0       TABLE
+        {10*(v(prod) - (Vtm*Ih))/(Vtm*Ih)} (0 0) (2 10)
Rlin     conmain 0       1meg

* Turn-on/Turn-off control
Eonoff   contot  0       TABLE
+        {v(congate)+v(conmain)+v(condvdt)} (0 0) (10 10)

* Turn-on/Turn-off delays
Rton    contot  dlay1   825
Dton    dlay1   control Delay
Rtoff   contot  dlay2   {290*Toff/Ton}
Dtoff   control dlay2   Delay
Cton    control 0       {Ton/454}

* Reverse breakdown
Dbreak  anode   break1  Dbreak
Dbreak2 cathode break1  Dseries

* Controlled switch model
.MODEL Vswitch vswitch
+ (Ron = {(Vtm-0.7)/Itm}, Roff = {Vdrm*Vdrm/(Vtm*Ih)},
+  Von = 5.0,             Voff = 1.5)

* Diodes
.MODEL  Dgk     D       (Is=1E-16 Cjo=50pf Rs=5)
.MODEL  Dseries D       (Is=1E-14)
.MODEL  Delay   D       (Is=1E-12 Cjo=5pf  Rs=0.01)
.MODEL  Dkarev  D       (Is=1E-10 Cjo=5pf  Rs=0.01)
.MODEL  Dakfwd  D       (Is=4E-11 Cjo=5pf)
.MODEL  Dbreak  D       (Ibv=1E-7 Bv={1.1*Vrrm} Cjo=5pf Rs=0.5)

* Allow the gate to float if required
Rfloat  gate    cathode 1e10
.ends

*$************************************************************************************

* thyristors used in RB_PC
.subckt RB_PC_2N6405    anode 	gate 	cathode

X1 anode gate cathode Scr params:
+ Vdrm = 800  
+ Vrrm = 800    
+ Ih   = 500e-3       
+ Vtm  = 1.26     
+ Itm  = 6240
+ dVdt = 1e9   
+ Igt  = 400e-3    
+ Vgt  = 3.5       
+ Ton  = 25e-6       
+ Toff = 500e-6
+ Idrm = 300e-3

*Personal Notes
* Vdrm --- Repetitive peak off-state : voltage maximum direct peak voltage
* Vrrm --- Repetitive peak off-state : voltage maximum reverse peak voltage
* Ih   --- Holding current           : current level at which the device turns off, without gate current
* Itm  --- Average on-state current  : maximum average current allowed
* Vtm  --- Peak forward voltage drop : voltage across a scr when it is conducting @ Itm
* dVdt --- Critical vlotage rise     : critical rate of rise of off-state voltage
* Igt  --- Triggering gate current
* Vgt  --- Triggering gate voltage
* Ton  --- Turn-on time              : Igate @ 10% , then wait Ton to have V A-K -10%
* Toff --- Turn-off time             : maximum working frequency
* Idrm --- Leakage current           : Maximum forward and reverse leakage current 

* 90-5-18    Motorola     DL137, Rev 2, 3/89
.ends

*$************************************************************************************

* thyristor used in RB_PC_RCFilter in crowbars branch
.subckt RB_PC_RCFilter_2N6405    anode 	gate 	cathode

X1 anode gate cathode Scr params:
+ Vdrm = 800
+ Vrrm = 800
+ Ih   = 150e-3
+ Vtm  = 1.33
+ Itm  = 134
+ dVdt = 1e9
+ Igt  = 100e-3
+ Vgt  = 2
+ Ton  = 25e-6
+ Toff = 500e-6
+ Idrm = 20e-3

*Personal Notes
* Vdrm --- Repetitive peak off-state : voltage maximum direct peak voltage
* Vrrm --- Repetitive peak off-state : voltage maximum reverse peak voltage
* Ih   --- Holding current           : current level at which the device turns off, without gate current
* Itm  --- Average on-state current  : maximum average current allowed
* Vtm  --- Peak forward voltage drop : voltage across a scr when it is conducting @ Itm
* dVdt --- Critical vlotage rise     : critical rate of rise of off-state voltage
* Igt  --- Triggering gate current
* Vgt  --- Triggering gate voltage
* Ton  --- Turn-on time              : Igate @ 10% , then wait Ton to have V Anode-Katode -10%
* Toff --- Turn-off time             : maximum working frequency
* Idrm --- Leakage current           : Maximum forward and reverse leakage current 

* 90-5-18    Motorola     DL137, Rev 2, 3/89
.ends

*$************************************************************************************

* RB_PC_PS_Thyr with control circuit and stabilizers
.subckt	RB_PC_PS_Thyr	   p1_In	p1_Out

x_thyr (p1_In gate p1_Out) RB_PC_2N6405
 
*Gate controller
r_gate     (gate 1)   10
I_gateCTRL (p1_Out 1) STIMULUS = I_GATE_RB_PC_PS_Thyr

r1_NS (p1_In 101)  10e-03
c1_NS (101 p1_Out) 1e-09
.ends

*$************************************************************************************

* RB_PC_Crow_Thyr with control circuit and stabilizers
.subckt	RB_PC_Crow_Thyr  p1_In	p1_Out

x_thyr (p1_In gate p1_Out) RB_PC_2N6405

*Gate controller
r_gate     (gate 1)   10
I_gateCTRL (p1_Out 1) STIMULUS=I_GATE_RB_PC_Crow_Thyr

r1_NS (p1_In 101)  1e-03
c1_NS (101 p1_Out) 1e-09
.ends

*$************************************************************************************

* RB_PC_RCFilter_Crow_Thyr with control circuit and stabilizers
.subckt	RB_PC_RCFilter_Crow_Thyr	p1_In	p1_Out

x_thyr (p1_In gate p1_Out) RB_PC_RCFilter_2N6405

*Gate controller
r_gate     (gate 1)   18
I_gateCTRL (p1_Out 1) STIMULUS=I_GATE_RB_PC_RCFilter_Crow_Thyr

r1_NS (p1_In 101)  1e-03
c1_NS (101 p1_Out) 1e-09
.ends

*$************************************************************************************
*$************************************************************************************
*$************************************************************************************
*$End of library