*************************************************************************************
* Library of CLIQ units and their switches
*************************************************************************************

*** Generic CLIQ unit
.subckt cliq_unit 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0

C_cliq (cpos cneg) {C_cliq}

xcliq_switch (cneg 1_pOut) cliq_switch
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch 1	2
+ PARAMS: 
+ R_cliq=0.05

S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends


*** In case there are more than one CLIQ unit in the same circuit,
* multiple library components are needed, governed by different stimuli
*** CLIQ unit #1
.subckt cliq_unit_1 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_1
+ PARAMS: R_cliq={R_cliq}
.ends

*** CLIQ unit #2
.subckt cliq_unit_2 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_2
+ PARAMS: R_cliq={R_cliq}
.ends

*** CLIQ unit #3
.subckt cliq_unit_3 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_3
+ PARAMS: R_cliq={R_cliq}
.ends

*** CLIQ unit #4
.subckt cliq_unit_4 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_4
+ PARAMS: R_cliq={R_cliq}
.ends

*** CLIQ unit #5
.subckt cliq_unit_5 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_5
+ PARAMS: R_cliq={R_cliq}
.ends

*** CLIQ unit #6
.subckt cliq_unit_6 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05

V_bb_in (1_pIn cpos) 0
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_6
+ PARAMS: R_cliq={R_cliq}
.ends

*****************************************


*** CLIQ unit #1 with additional self-inductance
.subckt cliq_unit_1_withInductance 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05
+ L_cliq=1n
V_bb_in (1_pIn 1ind) 0
L_cliq (1ind cpos) {L_cliq}
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_1
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch_1 1	2
+ PARAMS: 
+ R_cliq=0.05
S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control_1

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends

*** CLIQ unit #2 with additional self-inductance
.subckt cliq_unit_2_withInductance 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05
+ L_cliq=1n
V_bb_in (1_pIn 1ind) 0
L_cliq (1ind cpos) {L_cliq}
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_2
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch_2 1	2
+ PARAMS: 
+ R_cliq=0.05
S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control_2

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends

*** CLIQ unit #3 with additional self-inductance
.subckt cliq_unit_3_withInductance 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05
+ L_cliq=1n
V_bb_in (1_pIn 1ind) 0
L_cliq (1ind cpos) {L_cliq}
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_3
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch_3 1	2
+ PARAMS: 
+ R_cliq=0.05
S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control_3

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends

*** CLIQ unit #4 with additional self-inductance
.subckt cliq_unit_4_withInductance 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05
+ L_cliq=1n
V_bb_in (1_pIn 1ind) 0
L_cliq (1ind cpos) {L_cliq}
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_4
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch_4 1	2
+ PARAMS: 
+ R_cliq=0.05
S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control_4

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends

*** CLIQ unit #5 with additional self-inductance
.subckt cliq_unit_5_withInductance 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05
+ L_cliq=1n
V_bb_in (1_pIn 1ind) 0
L_cliq (1ind cpos) {L_cliq}
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_5
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch_5 1	2
+ PARAMS: 
+ R_cliq=0.05
S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control_5

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends

*** CLIQ unit #6 with additional self-inductance
.subckt cliq_unit_6_withInductance 1_pIn 1_pOut
+ PARAMS: 
+ C_cliq=0.04
+ R_cliq=0.05
+ L_cliq=1n
V_bb_in (1_pIn 1ind) 0
L_cliq (1ind cpos) {L_cliq}
C_cliq (cpos cneg) {C_cliq}
xcliq_switch (cneg 1_pOut) cliq_switch_6
+ PARAMS: R_cliq={R_cliq}
.ends

.subckt cliq_switch_6 1	2
+ PARAMS: 
+ R_cliq=0.05
S1    (1 2 control 0) xSmod
v_S1  (control 0) STIMULUS = V_cliq_control_6

.model xSmod Vswitch  Voff=0.0 Von=1.0  Roff=1e4   Ron={R_cliq}
.ends
