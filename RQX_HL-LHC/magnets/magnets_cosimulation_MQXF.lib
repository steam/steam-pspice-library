
*Subcircuit: One generic magnet with 4 electrical parts, named M1, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
* Parameters in the model:
* L_1 , L_2, L_3, L_4     self-inductances of each electrical part [H]
* k_1_2, k_1_3,...k_3_4   mutual coupling between electrical parts [H]
* C_ground_magnet         parasitic capacitance to ground of each magnet [F]
* R_parallel              resistance of the resistor in parallel to each magnet [Ohm]
* k_I                     factor to scale the self-inductance (default=1) [-]
* R_par                   virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt MAGNET_EQ_4_M1 1 2 3 4 5 6 7 8 
+ PARAMS:
+ L_1={1e-3}
+ L_2={1e-3}
+ L_3={1e-3}
+ L_4={1e-3}
+ k_1_2={0.000000}
+ k_1_3={0.000000}
+ k_1_4={0.000000}
+ k_2_3={0.000000}
+ k_2_4={0.000000}
+ k_3_4={0.000000}
+ k_I={1.000000}
+ R_par={1000.000000}

*
* Magnet electrical part 1
L_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_M1_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_M1_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_M1_1_stim
*
* Magnet electrical part 2
L_2 (3 2a) {L_2*k_I}
R_par_2 (3 2a) {R_par}
V_circuit_2	(2b 2a) STIMULUS = V_circuit_M1_2_stim
V_field_2	(2b 2c) STIMULUS = V_field_M1_2_stim
E_r_field_2 (2c 4) VALUE = {I(L_2)*V(2r)}
V_r_field_2	(2r 0) STIMULUS = R_field_M1_2_stim
*
* Magnet electrical part 3
L_3 (5 3a) {L_3*k_I}
R_par_3 (5 3a) {R_par}
V_circuit_3	(3b 3a) STIMULUS = V_circuit_M1_3_stim
V_field_3	(3b 3c) STIMULUS = V_field_M1_3_stim
E_r_field_3 (3c 6) VALUE = {I(L_3)*V(3r)}
V_r_field_3	(3r 0) STIMULUS = R_field_M1_3_stim
*
* Magnet electrical part 4
L_4 (7 4a) {L_4*k_I}
R_par_4 (7 4a) {R_par}
V_circuit_4	(4b 4a) STIMULUS = V_circuit_M1_4_stim
V_field_4	(4b 4c) STIMULUS = V_field_M1_4_stim
E_r_field_4 (4c 8) VALUE = {I(L_4)*V(4r)}
V_r_field_4	(4r 0) STIMULUS = R_field_M1_4_stim
*
* Mutual couplings
K_1_2 (L_1 L_2) {k_1_2}
K_1_3 (L_1 L_3) {k_1_3}
K_1_4 (L_1 L_4) {k_1_4}
K_2_3 (L_2 L_3) {k_2_3}
K_2_4 (L_2 L_4) {k_2_4}
K_3_4 (L_3 L_4) {k_3_4}
*
* Nodes to pickup the voltage across inductances
E_L_1 (1_v_l_diff 0) 1 1a 1.0
E_L_2 (2_v_l_diff 0) 3 2a 1.0
E_L_3 (3_v_l_diff 0) 5 3a 1.0
E_L_4 (4_v_l_diff 0) 7 4a 1.0
.ends


*Subcircuit: One generic magnet with 4 electrical parts, named M2, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
* Parameters in the model:
* L_1 , L_2, L_3, L_4     self-inductances of each electrical part [H]
* k_1_2, k_1_3,...k_3_4   mutual coupling between electrical parts [H]
* C_ground_magnet         parasitic capacitance to ground of each magnet [F]
* R_parallel              resistance of the resistor in parallel to each magnet [Ohm]
* k_I                     factor to scale the self-inductance (default=1) [-]
* R_par                   virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt MAGNET_EQ_4_M2 1 2 3 4 5 6 7 8 
+ PARAMS:
+ L_1={1e-3}
+ L_2={1e-3}
+ L_3={1e-3}
+ L_4={1e-3}
+ k_1_2={0.000000}
+ k_1_3={0.000000}
+ k_1_4={0.000000}
+ k_2_3={0.000000}
+ k_2_4={0.000000}
+ k_3_4={0.000000}
+ k_I={1.000000}
+ R_par={1000.000000}

*
* Magnet electrical part 1
L_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_M2_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_M2_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_M2_1_stim
*
* Magnet electrical part 2
L_2 (3 2a) {L_2*k_I}
R_par_2 (3 2a) {R_par}
V_circuit_2	(2b 2a) STIMULUS = V_circuit_M2_2_stim
V_field_2	(2b 2c) STIMULUS = V_field_M2_2_stim
E_r_field_2 (2c 4) VALUE = {I(L_2)*V(2r)}
V_r_field_2	(2r 0) STIMULUS = R_field_M2_2_stim
*
* Magnet electrical part 3
L_3 (5 3a) {L_3*k_I}
R_par_3 (5 3a) {R_par}
V_circuit_3	(3b 3a) STIMULUS = V_circuit_M2_3_stim
V_field_3	(3b 3c) STIMULUS = V_field_M2_3_stim
E_r_field_3 (3c 6) VALUE = {I(L_3)*V(3r)}
V_r_field_3	(3r 0) STIMULUS = R_field_M2_3_stim
*
* Magnet electrical part 4
L_4 (7 4a) {L_4*k_I}
R_par_4 (7 4a) {R_par}
V_circuit_4	(4b 4a) STIMULUS = V_circuit_M2_4_stim
V_field_4	(4b 4c) STIMULUS = V_field_M2_4_stim
E_r_field_4 (4c 8) VALUE = {I(L_4)*V(4r)}
V_r_field_4	(4r 0) STIMULUS = R_field_M2_4_stim
*
* Mutual couplings
K_1_2 (L_1 L_2) {k_1_2}
K_1_3 (L_1 L_3) {k_1_3}
K_1_4 (L_1 L_4) {k_1_4}
K_2_3 (L_2 L_3) {k_2_3}
K_2_4 (L_2 L_4) {k_2_4}
K_3_4 (L_3 L_4) {k_3_4}
*
* Nodes to pickup the voltage across inductances
E_L_1 (1_v_l_diff 0) 1 1a 1.0
E_L_2 (2_v_l_diff 0) 3 2a 1.0
E_L_3 (3_v_l_diff 0) 5 3a 1.0
E_L_4 (4_v_l_diff 0) 7 4a 1.0
.ends


*Subcircuit: One generic magnet with 4 electrical parts, named M3, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
* Parameters in the model:
* L_1 , L_2, L_3, L_4     self-inductances of each electrical part [H]
* k_1_2, k_1_3,...k_3_4   mutual coupling between electrical parts [H]
* C_ground_magnet         parasitic capacitance to ground of each magnet [F]
* R_parallel              resistance of the resistor in parallel to each magnet [Ohm]
* k_I                     factor to scale the self-inductance (default=1) [-]
* R_par                   virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt MAGNET_EQ_4_M3 1 2 3 4 5 6 7 8 
+ PARAMS:
+ L_1={1e-3}
+ L_2={1e-3}
+ L_3={1e-3}
+ L_4={1e-3}
+ k_1_2={0.000000}
+ k_1_3={0.000000}
+ k_1_4={0.000000}
+ k_2_3={0.000000}
+ k_2_4={0.000000}
+ k_3_4={0.000000}
+ k_I={1.000000}
+ R_par={1000.000000}

*
* Magnet electrical part 1
L_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_M3_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_M3_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_M3_1_stim
*
* Magnet electrical part 2
L_2 (3 2a) {L_2*k_I}
R_par_2 (3 2a) {R_par}
V_circuit_2	(2b 2a) STIMULUS = V_circuit_M3_2_stim
V_field_2	(2b 2c) STIMULUS = V_field_M3_2_stim
E_r_field_2 (2c 4) VALUE = {I(L_2)*V(2r)}
V_r_field_2	(2r 0) STIMULUS = R_field_M3_2_stim
*
* Magnet electrical part 3
L_3 (5 3a) {L_3*k_I}
R_par_3 (5 3a) {R_par}
V_circuit_3	(3b 3a) STIMULUS = V_circuit_M3_3_stim
V_field_3	(3b 3c) STIMULUS = V_field_M3_3_stim
E_r_field_3 (3c 6) VALUE = {I(L_3)*V(3r)}
V_r_field_3	(3r 0) STIMULUS = R_field_M3_3_stim
*
* Magnet electrical part 4
L_4 (7 4a) {L_4*k_I}
R_par_4 (7 4a) {R_par}
V_circuit_4	(4b 4a) STIMULUS = V_circuit_M3_4_stim
V_field_4	(4b 4c) STIMULUS = V_field_M3_4_stim
E_r_field_4 (4c 8) VALUE = {I(L_4)*V(4r)}
V_r_field_4	(4r 0) STIMULUS = R_field_M3_4_stim
*
* Mutual couplings
K_1_2 (L_1 L_2) {k_1_2}
K_1_3 (L_1 L_3) {k_1_3}
K_1_4 (L_1 L_4) {k_1_4}
K_2_3 (L_2 L_3) {k_2_3}
K_2_4 (L_2 L_4) {k_2_4}
K_3_4 (L_3 L_4) {k_3_4}
*
* Nodes to pickup the voltage across inductances
E_L_1 (1_v_l_diff 0) 1 1a 1.0
E_L_2 (2_v_l_diff 0) 3 2a 1.0
E_L_3 (3_v_l_diff 0) 5 3a 1.0
E_L_4 (4_v_l_diff 0) 7 4a 1.0
.ends


*Subcircuit: One generic magnet with 4 electrical parts, named M4, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
* Parameters in the model:
* L_1 , L_2, L_3, L_4     self-inductances of each electrical part [H]
* k_1_2, k_1_3,...k_3_4   mutual coupling between electrical parts [H]
* C_ground_magnet         parasitic capacitance to ground of each magnet [F]
* R_parallel              resistance of the resistor in parallel to each magnet [Ohm]
* k_I                     factor to scale the self-inductance (default=1) [-]
* R_par                   virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt MAGNET_EQ_4_M4 1 2 3 4 5 6 7 8 
+ PARAMS:
+ L_1={1e-3}
+ L_2={1e-3}
+ L_3={1e-3}
+ L_4={1e-3}
+ k_1_2={0.000000}
+ k_1_3={0.000000}
+ k_1_4={0.000000}
+ k_2_3={0.000000}
+ k_2_4={0.000000}
+ k_3_4={0.000000}
+ k_I={1.000000}
+ R_par={1000.000000}

*
* Magnet electrical part 1
L_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_M4_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_M4_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_M4_1_stim
*
* Magnet electrical part 2
L_2 (3 2a) {L_2*k_I}
R_par_2 (3 2a) {R_par}
V_circuit_2	(2b 2a) STIMULUS = V_circuit_M4_2_stim
V_field_2	(2b 2c) STIMULUS = V_field_M4_2_stim
E_r_field_2 (2c 4) VALUE = {I(L_2)*V(2r)}
V_r_field_2	(2r 0) STIMULUS = R_field_M4_2_stim
*
* Magnet electrical part 3
L_3 (5 3a) {L_3*k_I}
R_par_3 (5 3a) {R_par}
V_circuit_3	(3b 3a) STIMULUS = V_circuit_M4_3_stim
V_field_3	(3b 3c) STIMULUS = V_field_M4_3_stim
E_r_field_3 (3c 6) VALUE = {I(L_3)*V(3r)}
V_r_field_3	(3r 0) STIMULUS = R_field_M4_3_stim
*
* Magnet electrical part 4
L_4 (7 4a) {L_4*k_I}
R_par_4 (7 4a) {R_par}
V_circuit_4	(4b 4a) STIMULUS = V_circuit_M4_4_stim
V_field_4	(4b 4c) STIMULUS = V_field_M4_4_stim
E_r_field_4 (4c 8) VALUE = {I(L_4)*V(4r)}
V_r_field_4	(4r 0) STIMULUS = R_field_M4_4_stim
*
* Mutual couplings
K_1_2 (L_1 L_2) {k_1_2}
K_1_3 (L_1 L_3) {k_1_3}
K_1_4 (L_1 L_4) {k_1_4}
K_2_3 (L_2 L_3) {k_2_3}
K_2_4 (L_2 L_4) {k_2_4}
K_3_4 (L_3 L_4) {k_3_4}
*
* Nodes to pickup the voltage across inductances
E_L_1 (1_v_l_diff 0) 1 1a 1.0
E_L_2 (2_v_l_diff 0) 3 2a 1.0
E_L_3 (3_v_l_diff 0) 5 3a 1.0
E_L_4 (4_v_l_diff 0) 7 4a 1.0
.ends


*Subcircuit: One generic magnet with 4 electrical parts, named M5, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
* Parameters in the model:
* L_1 , L_2, L_3, L_4     self-inductances of each electrical part [H]
* k_1_2, k_1_3,...k_3_4   mutual coupling between electrical parts [H]
* C_ground_magnet         parasitic capacitance to ground of each magnet [F]
* R_parallel              resistance of the resistor in parallel to each magnet [Ohm]
* k_I                     factor to scale the self-inductance (default=1) [-]
* R_par                   virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt MAGNET_EQ_4_M5 1 2 3 4 5 6 7 8 
+ PARAMS:
+ L_1={1e-3}
+ L_2={1e-3}
+ L_3={1e-3}
+ L_4={1e-3}
+ k_1_2={0.000000}
+ k_1_3={0.000000}
+ k_1_4={0.000000}
+ k_2_3={0.000000}
+ k_2_4={0.000000}
+ k_3_4={0.000000}
+ k_I={1.000000}
+ R_par={1000.000000}

*
* Magnet electrical part 1
L_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_M5_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_M5_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_M5_1_stim
*
* Magnet electrical part 2
L_2 (3 2a) {L_2*k_I}
R_par_2 (3 2a) {R_par}
V_circuit_2	(2b 2a) STIMULUS = V_circuit_M5_2_stim
V_field_2	(2b 2c) STIMULUS = V_field_M5_2_stim
E_r_field_2 (2c 4) VALUE = {I(L_2)*V(2r)}
V_r_field_2	(2r 0) STIMULUS = R_field_M5_2_stim
*
* Magnet electrical part 3
L_3 (5 3a) {L_3*k_I}
R_par_3 (5 3a) {R_par}
V_circuit_3	(3b 3a) STIMULUS = V_circuit_M5_3_stim
V_field_3	(3b 3c) STIMULUS = V_field_M5_3_stim
E_r_field_3 (3c 6) VALUE = {I(L_3)*V(3r)}
V_r_field_3	(3r 0) STIMULUS = R_field_M5_3_stim
*
* Magnet electrical part 4
L_4 (7 4a) {L_4*k_I}
R_par_4 (7 4a) {R_par}
V_circuit_4	(4b 4a) STIMULUS = V_circuit_M5_4_stim
V_field_4	(4b 4c) STIMULUS = V_field_M5_4_stim
E_r_field_4 (4c 8) VALUE = {I(L_4)*V(4r)}
V_r_field_4	(4r 0) STIMULUS = R_field_M5_4_stim
*
* Mutual couplings
K_1_2 (L_1 L_2) {k_1_2}
K_1_3 (L_1 L_3) {k_1_3}
K_1_4 (L_1 L_4) {k_1_4}
K_2_3 (L_2 L_3) {k_2_3}
K_2_4 (L_2 L_4) {k_2_4}
K_3_4 (L_3 L_4) {k_3_4}
*
* Nodes to pickup the voltage across inductances
E_L_1 (1_v_l_diff 0) 1 1a 1.0
E_L_2 (2_v_l_diff 0) 3 2a 1.0
E_L_3 (3_v_l_diff 0) 5 3a 1.0
E_L_4 (4_v_l_diff 0) 7 4a 1.0
.ends


*Subcircuit: One generic magnet with 4 electrical parts, named M6, modeled with field/circuit coupling (SPICE+LEDET co-simulation)
* Parameters in the model:
* L_1 , L_2, L_3, L_4     self-inductances of each electrical part [H]
* k_1_2, k_1_3,...k_3_4   mutual coupling between electrical parts [H]
* C_ground_magnet         parasitic capacitance to ground of each magnet [F]
* R_parallel              resistance of the resistor in parallel to each magnet [Ohm]
* k_I                     factor to scale the self-inductance (default=1) [-]
* R_par                   virtual parallel resistance to help convergence (default=1000 Ohm) [Ohm]
.subckt MAGNET_EQ_4_M6 1 2 3 4 5 6 7 8 
+ PARAMS:
+ L_1={1e-3}
+ L_2={1e-3}
+ L_3={1e-3}
+ L_4={1e-3}
+ k_1_2={0.000000}
+ k_1_3={0.000000}
+ k_1_4={0.000000}
+ k_2_3={0.000000}
+ k_2_4={0.000000}
+ k_3_4={0.000000}
+ k_I={1.000000}
+ R_par={1000.000000}

*
* Magnet electrical part 1
L_1 (1 1a) {L_1*k_I}
R_par_1 (1 1a) {R_par}
V_circuit_1	(1b 1a) STIMULUS = V_circuit_M6_1_stim
V_field_1	(1b 1c) STIMULUS = V_field_M6_1_stim
E_r_field_1 (1c 2) VALUE = {I(L_1)*V(1r)}
V_r_field_1	(1r 0) STIMULUS = R_field_M6_1_stim
*
* Magnet electrical part 2
L_2 (3 2a) {L_2*k_I}
R_par_2 (3 2a) {R_par}
V_circuit_2	(2b 2a) STIMULUS = V_circuit_M6_2_stim
V_field_2	(2b 2c) STIMULUS = V_field_M6_2_stim
E_r_field_2 (2c 4) VALUE = {I(L_2)*V(2r)}
V_r_field_2	(2r 0) STIMULUS = R_field_M6_2_stim
*
* Magnet electrical part 3
L_3 (5 3a) {L_3*k_I}
R_par_3 (5 3a) {R_par}
V_circuit_3	(3b 3a) STIMULUS = V_circuit_M6_3_stim
V_field_3	(3b 3c) STIMULUS = V_field_M6_3_stim
E_r_field_3 (3c 6) VALUE = {I(L_3)*V(3r)}
V_r_field_3	(3r 0) STIMULUS = R_field_M6_3_stim
*
* Magnet electrical part 4
L_4 (7 4a) {L_4*k_I}
R_par_4 (7 4a) {R_par}
V_circuit_4	(4b 4a) STIMULUS = V_circuit_M6_4_stim
V_field_4	(4b 4c) STIMULUS = V_field_M6_4_stim
E_r_field_4 (4c 8) VALUE = {I(L_4)*V(4r)}
V_r_field_4	(4r 0) STIMULUS = R_field_M6_4_stim
*
* Mutual couplings
K_1_2 (L_1 L_2) {k_1_2}
K_1_3 (L_1 L_3) {k_1_3}
K_1_4 (L_1 L_4) {k_1_4}
K_2_3 (L_2 L_3) {k_2_3}
K_2_4 (L_2 L_4) {k_2_4}
K_3_4 (L_3 L_4) {k_3_4}
*
* Nodes to pickup the voltage across inductances
E_L_1 (1_v_l_diff 0) 1 1a 1.0
E_L_2 (2_v_l_diff 0) 3 2a 1.0
E_L_3 (3_v_l_diff 0) 5 3a 1.0
E_L_4 (4_v_l_diff 0) 7 4a 1.0
.ends



*$************************************************************************************
*$End of library