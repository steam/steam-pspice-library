
* PSPICE RQ Power Converter Main Filter
* 05/07/2019 CERN
* Dimitri Pracht


*$**********************************************************************************


*SHORT DESCRIPTION OF THE MODULE:

*------------------------------------------------------------------------------------------------------------------------
* This module contains the main output filter used within the RDF/D circuit.

*------------------------------------------------------------------------------------------------------------------------

*------------------------------------------------------------------------------------------------------------------------

*The full description of the circuit is available at the following link: 

* -> https://twiki.cern.ch/twiki/bin/viewauth/TEMPEPE/SectionThesis

* --> on this Link go to: D. Pracht "Multiphysics modelling of the LHC main quadrupole superconducting circuit"

* ---> within the document go to: Chapter 5 "LHC main quadrupole electrical circuit model". 

*------------------------------------------------------------------------------------------------------------------------


.subckt	RQ_PC_FILTER_MAIN	1_pIn_I 2_pIn_I 1_pOut_I_pos 2_pOut_I_neg
*+ R_Cable = {960e-6}


v_monitor_1	(1_pIn_I 101) 0
v_monitor_2	(2_pIn_I 201) 0
*------------------------------------


r_addition_pos_1_1_earth (101 0) 1e6
c_addition_pos_1_1_earth (101 0) 9e-6

r_addition_pos_1_2_earth (101 0) 1e6
c_addition_pos_1_2_earth (101 0) 9e-6


r_addition_neg_1_3_earth (201 0) 1e6
c_addition_neg_1_3_earth (201 0) 9e-6

r_addition_neg_1_4_earth (201 0) 1e6
c_addition_neg_1_4_earth (201 0) 9e-6


c_filter_1_1		(101 201) 18e-6


r_addition_pos_1_5_earth (101 0) 1e6
c_addition_pos_1_5_earth (101 0) 9e-6

r_addition_neg_1_6_earth (201 0) 1e6
c_addition_neg_1_6_earth (201 0) 9e-6


c_filter_1_2		(101 201) 18e-6


r_addition_pos_1_7_earth (101 0) 1e6
c_addition_pos_1_7_earth (101 0) 9e-6

r_addition_pos_1_8_earth  (101 0) 1e6
c_addition_pos_1_8_earth (101 0) 9e-6


r_addition_neg_1_9_earth (201 0) 1e6
c_addition_neg_1_9_earth (201 0) 9e-6

r_addition_neg_1_10_earth  (201 0) 1e6
c_addition_neg_1_10_earth (201 0) 9e-6



r_addition_pos_1_earth (101 0) 1e6
c_addition_pos_1_earth (101 0) 2.2e-6


x_D1 (101A 101) 440CNQ030_test
x_D2 (101A 101) 440CNQ030_test

r_monitoring_card_1_2 (201 101A) 150e-6


x_D3 (201A 101) 440CNQ030_test
x_D4 (201A 101) 440CNQ030_test

r_monitoring_card_3_4 (201 201A) 150e-6



x_D5 (301A 101) 440CNQ030_test
x_D6 (301A 101) 440CNQ030_test

r_monitoring_card_5_6 (201 301A) 150e-6


x_D7 (401A 101) 440CNQ030_test
x_D8 (401A 101) 440CNQ030_test


r_monitoring_card_7_8 (201 401A) 150e-6


*r_addition_pos_1_13_earth (101 0) 1e6
*c_addition_pos_1_14_earth (101 0) 2.2e-6


x_D9 (501A 101) 440CNQ030_test
x_D10 (501A 101) 440CNQ030_test


r_monitoring_card_9_10 (201 501A) 150e-6



x_D11 (601A 101) 440CNQ030_test
x_D12 (601A 101) 440CNQ030_test

r_monitoring_card_11_12 (201 601A) 150e-6




x_D13 (701A 101) 440CNQ030_test
x_D14 (701A 101) 440CNQ030_test

r_monitoring_card_13_14 (201 701A) 150e-6



x_D15 (801A 101) 440CNQ030_test
x_D16 (801A 101) 440CNQ030_test


r_monitoring_card_15_16 (201 801A) 150e-6

*r_addition_pos_1_13_earth (101 0) 1e6
*c_addition_pos_1_14_earth (101 0) 2.2e-6


*r_addition_pos_1_15_earth (101 0) 1e6
*c_addition_pos_1_16_earth (101 0) 2.2e-6

x_D17 (901A 101) 440CNQ030_test
x_D18 (901A 101) 440CNQ030_test


r_monitoring_card_17_18 (201 901A) 150e-6


x_D19 (1001A 101) 440CNQ030_test
x_D20 (1001A 101) 440CNQ030_test

r_monitoring_card_19_20 (201 1001A) 150e-6



x_D21 (1101A 101) 440CNQ030_test
x_D22 (1101A 101) 440CNQ030_test

r_monitoring_card_21_22 (201 1101A) 150e-6



x_D23 (1201A 101) 440CNQ030_test
x_D24 (1201A 101) 440CNQ030_test


r_monitoring_card_23_24 (201 1201A) 150e-6

*r_addition_pos_1_17_earth (101 0) 1e6
*c_addition_pos_1_18_earth (101 0) 2.2e-6

x_D25 (1301A 101) 440CNQ030_test
x_D26 (1301A 101) 440CNQ030_test

r_monitoring_card_25_26 (201 1301A) 150e-6


x_D27 (1401A 101) 440CNQ030_test
x_D28 (1401A 101) 440CNQ030_test


r_monitoring_card_27_28 (201 1401A) 150e-6



x_D29 (1501A 101) 440CNQ030_test
x_D30 (1501A 101) 440CNQ030_test

r_monitoring_card_29_30 (201 1501A) 150e-6


x_D31 (1601A 101) 440CNQ030_test
x_D32 (1601A 101) 440CNQ030_test


r_monitoring_card_31_32 (201 1601A) 150e-6


r_addition_pos_2_earth (101 0) 1e6
c_addition_pos_2_earth (101 0) 2.2e-6

r_addition_pos_3_earth (101 0) 1e6
c_addition_pos_3_earth (101 0) 2.2e-6


x_D33 (1701A 101) 440CNQ030_test
x_D34 (1701A 101) 440CNQ030_test

r_monitoring_card_33_34 (201 1701A) 150e-6


x_D35 (1801A 101) 440CNQ030_test
x_D36 (1801A 101) 440CNQ030_test

r_monitoring_card_35_36 (201 1801A) 150e-6

x_D37 (1901A 101) 440CNQ030_test
x_D38 (1901A 101) 440CNQ030_test

r_monitoring_card_37_38 (201 1901A) 150e-6


x_D39 (2001A 101) 440CNQ030_test
x_D40 (2001A 101) 440CNQ030_test


r_monitoring_card_39_40 (201 2001A) 150e-6


*r_addition_pos_1_20_earth (101 0) 1e6
*c_addition_pos_1_21_earth (101 0) 2.2e-6


x_D41 (2101A 101) 440CNQ030_test
x_D42 (2101A 101) 440CNQ030_test

r_monitoring_card_41_42 (201 2101A) 150e-6


x_D43 (2201A 101) 440CNQ030_test
x_D44 (2201A 101) 440CNQ030_test

r_monitoring_card_43_44 (201 2201A) 150e-6



x_D45 (2301A 101) 440CNQ030_test
x_D46 (2301A 101) 440CNQ030_test

r_monitoring_card_45_46 (201 2301A) 150e-6

x_D47 (2401A 101) 440CNQ030_test
x_D48 (2401A 101) 440CNQ030_test


r_monitoring_card_47_48 (201 2401A) 150e-6

*r_addition_pos_1_22_earth (101 0) 1e6
*c_addition_pos_1_23_earth (101 0) 2.2e-6

x_D49 (2501A 101) 440CNQ030_test
x_D50 (2501A 101) 440CNQ030_test

r_monitoring_card_49_50 (201 2501A) 150e-6


x_D51 (2601A 101) 440CNQ030_test
x_D52 (2601A 101) 440CNQ030_test

r_monitoring_card_51_52 (201 2601A) 150e-6

x_D53 (2701A 101) 440CNQ030_test
x_D54 (2701A 101) 440CNQ030_test

r_monitoring_card_53_54 (201 2701A) 150e-6


x_D55 (2801A 101) 440CNQ030_test
x_D56 (2801A 101) 440CNQ030_test


r_monitoring_card_55_56 (201 2801A) 150e-6

*r_addition_pos_1_24_earth (101 0) 1e6
*c_addition_pos_1_25_earth (101 0) 2.2e-6

x_D57 (2901A 101) 440CNQ030_test
x_D58 (2901A 101) 440CNQ030_test

r_monitoring_card_57_58 (201 2901A) 150e-6


x_D59 (3001A 101) 440CNQ030_test
x_D60 (3001A 101) 440CNQ030_test

r_monitoring_card_59_60 (201 3001A) 150e-6


x_D61 (3101A 101) 440CNQ030_test
x_D62 (3101A 101) 440CNQ030_test

r_monitoring_card_61_62 (201 3101A) 150e-6


x_D63 (3201A 101) 440CNQ030_test
x_D64 (3201A 101) 440CNQ030_test


r_monitoring_card_63_64 (201 3201A) 150e-6


r_addition_pos_4_earth (101 0) 1e6
c_addition_pos_4_earth (101 0) 2.2e-6

x_D65 (3301A 101) 1_SKN6000_test
x_D66 (3301A 101) 1_SKN6000_test
x_D67 (3301A 101) 1_SKN6000_test

r_monitoring_card_9 (201 3301A) 100e-12

v_dcct (3401A 201) 0

r_addition_neg_5_earth (3401A 0) 1e6
c_addition_neg_5_earth (3401A 0) 2.2e-6
r_addition_neg_6_earth (3401A 0) 1e6
c_addition_neg_6_earth (3401A 0) 2.2e-6
r_addition_neg_7_earth (3401A 0) 1e6
c_addition_neg_7_earth (3401A 0) 2.2e-6
r_addition_neg_8_earth (3401A 0) 1e6
c_addition_neg_8_earth (3401A 0) 2.2e-6

c_filter_1_3		(101 3401A) 10e-6
c_filter_1_4		(101 3401A) 10e-6
c_filter_1_5		(101 3401A) 10e-6
c_filter_1_6		(101 3401A) 10e-6

c_filter_1_7		(101 3401A) 10e-6
c_filter_1_8		(101 3401A) 10e-6
c_filter_1_9		(101 3401A) 10e-6
c_filter_1_10		(101 3401A) 10e-6

c_filter_1_11		(101 3401A) 10e-6
c_filter_1_12		(101 3401A) 10e-6
c_filter_1_13		(101 3401A) 10e-6
c_filter_1_14		(101 3401A) 10e-6

c_filter_1_15		(101 3401A) 10e-6
c_filter_1_16		(101 3401A) 10e-6
c_filter_1_17		(101 3401A) 10e-6
c_filter_1_18		(101 3401A) 10e-6

c_filter_1_19		(101 3401A) 10e-6
c_filter_1_20		(101 3401A) 10e-6

*R_Cable_to_MAG001   (101 102) 480e-6

*R_Cable_to_Switch2  (201 202) 480e-6

*---------------------------------------

v_monitor_3  (101 1_pOut_I_pos)    0

v_monitor_4  (3401A 2_pOut_I_neg)    0

.ends

*$************************************************************************************
*
*$************************************************************************************
*$************************************************************************************
*$************************************************************************************
*$End of library

