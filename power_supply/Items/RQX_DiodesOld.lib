
* PSPICE RQX diodes library
* Jan 2016 CERN
* MPrioli

*$************************************************************************************

* Diode used in FWD circuit:
* Parallel with Q3 in conf. A
* Parallel with Q1 in conf. B

.MODEL D4457Nnew D
+ IS  = 716.11E-6
+ N   = 1.5855
+ RS  = 22.185E-6
+ IKF = 1.2164
+ CJO = 1.0000E-12
+ M   = .3333
+ VJ  = .75
+ ISR = 100.00E-12
+ BV  = 100
+ IBV = 100.00E-6
+ TT  = 5.0000E-9
*$

*$************************************************************************************

* Diode in parallel to RQX
* Diode type: 245NQ015R at approx 25deg
* Diode manufacturer: International Rectifier
* Values calculated using AMS model editor; datasheet curves
* Date: 16/06/2011 SROWAN

.model D245NQ015R25deg D
+ IS  = 2.13186E-02
+ N   = 1.55E+00
+ RS  = 2.37E-04
+ IKF = 2.60259E+02
+ XTI = 3.00000E+00
+ EG  = 1.11
+ CJO = 2.85375E-09
+ M   = 3.02598E-01
+ VJ  = 3.90500E-01
+ FC  = 5.00000E-01
+ ISR = 0.001
+ NR  = 2
+ BV  = 1.50000E+01
+ IBV = 4.00000E-02
+ TT  = 1.00000E-09
+ KF  = 0
+ AF  = 1
*$

*$************************************************************************************

* Diode in parallel to RTQX2

.MODEL D245RTQX2 D
+ IS  = 2.13186E-02
*+ IS  = 2.13186E-01
+ N   = 1.15E+00
*+ N   = 1.55E+00
+ RS  = 4.85E-04
*+ RS  = 2.37E-04
+ IKF = 2.60259E+02
+ XTI = 3.00000E+00
+ EG  = 1.11
+ CJO = 2.85375E-09
+ M   = 3.02598E-01
+ VJ  = 3.90500E-01
+ FC  = 5.00000E-01
+ ISR = 0.001
+ NR  = 2
+ BV  = 1.50000E+01
+ IBV = 4.00000E-02
+ TT  = 1.00000E-09
+ KF  = 0
+ AF  = 1
*$

*$************************************************************************************

* FWD circuit -- Simulink Schematics Available:
* Parallel with Q3 in conf. A
* Parallel with Q1 in conf. B

.subckt FWD 	1_pIn	1_pOut

v1_PH (1_pIn 100) 0

l_par (100 101) 10u

xr11_bb   (101 102)	RQX_BB	PARAMS: length = 9  section = 240
xr1_LEM   (101 102)	RQX_BB	PARAMS: length = 9  section = 25
D1		  (102 103)	D4457Nnew
xr12_bb   (103 104)	RQX_BB	PARAMS: length = 9  section = 240

xr21_bb   (101 202)	RQX_BB	PARAMS: length = 9  section = 240
xr2_LEM   (101 202)	RQX_BB	PARAMS: length = 9  section = 25
D2		  (202 203)	D4457Nnew
xr22_bb   (203 104)	RQX_BB	PARAMS: length = 9  section = 240

v2_PH (104 1_pOut) 0

.ends

*$************************************************************************************

* Stack of diodes in parallel to RQX -- Simulink Schematics Available:

.subckt RQX_DIODE 	1_pIn	1_pOut

v1_PH (1_pIn 100) 0

r_par (100 101) 10n

D1  (101 102)	D245NQ015R25deg
D2  (101 102)	D245NQ015R25deg
D3  (101 102)	D245NQ015R25deg
D4  (101 102)	D245NQ015R25deg
D5  (101 102)	D245NQ015R25deg
D6  (101 102)	D245NQ015R25deg
D7  (101 102)	D245NQ015R25deg
D8  (101 102)	D245NQ015R25deg
D9  (101 102)	D245NQ015R25deg
D10 (101 102)	D245NQ015R25deg
D11 (101 102)	D245NQ015R25deg
D12 (101 102)	D245NQ015R25deg
D13 (101 102)	D245NQ015R25deg
D14 (101 102)	D245NQ015R25deg
D15 (101 102)	D245NQ015R25deg
D16 (101 102)	D245NQ015R25deg
D17 (101 102)	D245NQ015R25deg
D18 (101 102)	D245NQ015R25deg
D19 (101 102)	D245NQ015R25deg
D20 (101 102)	D245NQ015R25deg

D21 (101 102)	D245NQ015R25deg
D22 (101 102)	D245NQ015R25deg
D23 (101 102)	D245NQ015R25deg
D24 (101 102)	D245NQ015R25deg
D25 (101 102)	D245NQ015R25deg
D26 (101 102)	D245NQ015R25deg
D27 (101 102)	D245NQ015R25deg
D28 (101 102)	D245NQ015R25deg
D29 (101 102)	D245NQ015R25deg
D30 (101 102)	D245NQ015R25deg
D31 (101 102)	D245NQ015R25deg
D32 (101 102)	D245NQ015R25deg
D33 (101 102)	D245NQ015R25deg
D34 (101 102)	D245NQ015R25deg
D35 (101 102)	D245NQ015R25deg
D36 (101 102)	D245NQ015R25deg
D37 (101 102)	D245NQ015R25deg
D38 (101 102)	D245NQ015R25deg
D39 (101 102)	D245NQ015R25deg
D40 (101 102)	D245NQ015R25deg

v2_PH (102 1_pOut) 0

.ends

*$************************************************************************************

* Stack of diodes in parallel to RTQX2 -- Simulink Schematics Available:

.subckt RTQX2_DIODE 	1_pIn	1_pOut

v1_PH (1_pIn 100) 0

r_par (100 101) 10n

D1  (101 102)	D245RTQX2
D2  (101 102)	D245RTQX2
D3  (101 102)	D245RTQX2
D4  (101 102)	D245RTQX2
D5  (101 102)	D245RTQX2
D6  (101 102)	D245RTQX2
D7  (101 102)	D245RTQX2
D8  (101 102)	D245RTQX2
D9  (101 102)	D245RTQX2
D10 (101 102)	D245RTQX2
D11 (101 102)	D245RTQX2
D12 (101 102)	D245RTQX2
D13 (101 102)	D245RTQX2
D14 (101 102)	D245RTQX2
D15 (101 102)	D245RTQX2
D16 (101 102)	D245RTQX2
D17 (101 102)	D245RTQX2
D18 (101 102)	D245RTQX2
D19 (101 102)	D245RTQX2
D20 (101 102)	D245RTQX2

v2_PH (102 1_pOut) 0

.ends

*$************************************************************************************

